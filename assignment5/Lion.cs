﻿using System;
using System.Collections.Generic;
using System.Text;

namespace assignment5
{
    public class Lion : Cat
    {
        public Lion()
        {
            catRace = nameof(Lion);
        }
        public  virtual  void HatesWater()
        {
            Console.WriteLine("Lions hate the water and don't take a bath");
        }
    }
}
