﻿using System;
using System.Collections.Generic;
using System.Text;

namespace assignment5
{
    public class Puddle : Dog, ISitInPurse
    {
        public Puddle()
        {
            dogRace = nameof(Puddle);
        }
        public  virtual  void SitInHandBag()
        {
            Console.WriteLine("Sits in the hand bag of woman");
        }
        public  virtual  void PlayDressUp()
        {
            Console.WriteLine("The owner loves to play dress up with this puddle");
        }
        public  void SitingInPurse(string someWord)
        {
            someWord = "Yippi";
            Console.WriteLine("this is really " + someWord);
        }
    }
}
